from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
import time

#Initialize 
driver = webdriver.Chrome()
#Navigate
url = 'https://www.linkedin.com/jobs/'
time.sleep(5)
wait = WebDriverWait(driver, 10)
job_posts = []
page = 1
post = 1
while True:
    try:
        print('While True is True...')
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        print('Begin extraction ...')
        #  Extract job posts per page
        print(f'Page: {page} -Post: {post}')
        for card in soup.find_all('div', class_='flex-grow-1 artdeco-entity-lockup__content ember-view'):
            title = card.find('a', class_='ember-view job-card-container__link job-card-list__title').text.strip()
            company = card.find('h2', class_='job-card-container__primary-description ').text.strip()
           # description = card.find('p', class_='card-offer__description').text.strip()
            location = card.find_all('ul',class_='job-card-container__metadata-wrapper')
            etat = card.find('div',class_='job-card-container__job-insight-text')
            publication = card.find_all('ul',alt='job-card-list__footer-wrapper job-card-container__footer-wrapper flex-shrink-zero display-flex t-sans t-12 t-black--light t-normal t-roman')
           # contract_type = details[1].find('img',alt='type de contrat').parent.text.strip() if len(details) > 1 else 'N/A'
            #location = details[1].find('img',alt='localisation').parent.text.strip() if len(details) > 1 else 'N/A'
           # publication = details[1].find('img',alt='date de publication').parent.text.strip() if len(details) > 1 else 'N/A'
            post += 1
            print(f'Title: {title}\nCompany : {company}\nLocation:{location}\nDate:{publication}')
            job_posts.append({
                'Title':title,
                'Company':company,
              #  'Description':description,
                #'Salary':salary,
                #'contrat':contract_type,
                'Location':location,
                'etat':etat,
                'Date':publication
            })
    except:
        print('Could not scrap...')

    page +=1
    try:
        button = WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable((By.XPATH,'//a[contains(text(), "Suiv.")]'))
        )
        button.click()
        print(f'Page number {page}')
        time.sleep(8)

    except Exception as e:
        print(f'Error encounterd: {e}')
        break
df=pd.DataFrame(job_posts)
print('Head rows\n----------')
print(df.head())
print('Tail rows\n----------')
print(df.tail())
df.to_csv('job_post_4612.csv', index=False)
driver.quit()